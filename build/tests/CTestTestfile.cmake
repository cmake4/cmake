# CMake generated Testfile for 
# Source directory: /home/padawa/cpp/cpp-boilerplate/tests
# Build directory: /home/padawa/cpp/cpp-boilerplate/build/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(BP.failtest "/home/padawa/cpp/cpp-boilerplate/build/tests/failtest" "--force-colors=true")
set_tests_properties(BP.failtest PROPERTIES  WILL_FAIL "TRUE" _BACKTRACE_TRIPLES "/home/padawa/cpp/cpp-boilerplate/tests/CMakeLists.txt;11;add_test;/home/padawa/cpp/cpp-boilerplate/tests/CMakeLists.txt;0;")
add_test(BP.successtest "/home/padawa/cpp/cpp-boilerplate/build/tests/successtest" "--force-colors=true")
set_tests_properties(BP.successtest PROPERTIES  _BACKTRACE_TRIPLES "/home/padawa/cpp/cpp-boilerplate/tests/CMakeLists.txt;25;add_test;/home/padawa/cpp/cpp-boilerplate/tests/CMakeLists.txt;0;")
