# CMake generated Testfile for 
# Source directory: /home/padawa/cpp/cpp-boilerplate/external
# Build directory: /home/padawa/cpp/cpp-boilerplate/build/external
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("fmt")
subdirs("spdlog")
