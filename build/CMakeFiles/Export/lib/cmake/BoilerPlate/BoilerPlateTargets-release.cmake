#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "BoilerPlate::BoilerPlate" for configuration "Release"
set_property(TARGET BoilerPlate::BoilerPlate APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(BoilerPlate::BoilerPlate PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/BoilerPlate"
  )

list(APPEND _IMPORT_CHECK_TARGETS BoilerPlate::BoilerPlate )
list(APPEND _IMPORT_CHECK_FILES_FOR_BoilerPlate::BoilerPlate "${_IMPORT_PREFIX}/bin/BoilerPlate" )

# Import target "BoilerPlate::bp_foo" for configuration "Release"
set_property(TARGET BoilerPlate::bp_foo APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(BoilerPlate::bp_foo PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libbp_foo.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS BoilerPlate::bp_foo )
list(APPEND _IMPORT_CHECK_FILES_FOR_BoilerPlate::bp_foo "${_IMPORT_PREFIX}/lib/libbp_foo.a" )

# Import target "BoilerPlate::fmt" for configuration "Release"
set_property(TARGET BoilerPlate::fmt APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(BoilerPlate::fmt PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libfmt.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS BoilerPlate::fmt )
list(APPEND _IMPORT_CHECK_FILES_FOR_BoilerPlate::fmt "${_IMPORT_PREFIX}/lib/libfmt.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
